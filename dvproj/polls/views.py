from django.http import HttpResponse, Http404

from django.shortcuts import render

from .models import Question, Choice

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    text_and_links_list = []
    for q in latest_question_list:
        choice_list = Choice.objects.filter(question=q.id)
        chcount = 0
        if choice_list:
            chcount = len(choice_list)
        href_open = '<a href="/polls/%s/detail">' % q.id
        textplus = '%s%s</a> (choices: %s)' % (href_open,q.question_text,chcount)
        text_and_links_list.append(textplus)
    para1str = ', <br />'.join(text_and_links_list)
    cdict = {'para1': para1str,
             'html_title': 'Question summary / index',
            }
    return render(request, 'basepara1.html', context=cdict)

def detail(request, question_id):
    try:
        q = Question.objects.get(id=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    choice_list = Choice.objects.filter(question=q.id)
    chcount = 0
    if choice_list:
        chcount = len(choice_list)
    para1str = 'Question %s: %s<br />' % (question_id,q.question_text)
    if chcount < 1:
        para1str+="  ( choices are not yet entered for this question )<br />"
    else:
        for c in choice_list:
            para1str+="  choice: %s<br />" % c.choice_text
    cdict = {'para1': para1str,
             'html_title': 'Question Detail',
            }
    return render(request, 'basepara1.html', context=cdict)

def results(request, question_id):
    try:
        text = Question.objects.get(id=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    para1str = 'Results of Question %s: %s<br />' % (question_id,text)
    return HttpResponse(para1str)

def vote(request, question_id):
    """ Return a suitable response to urls like localhost:8000/polls/1/vote/ """
    body1str = "<p>You are <em>voting</em> on question %s.</p>" % question_id
    cdict = {'body1': body1str}
    #return HttpResponse(output)
    return render(request, 'basebody1.html', context=cdict)

