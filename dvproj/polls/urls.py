from django.conf.urls import url
#from django.urls import path, re_path	# django 2 and newer only
#from django.urls import include

from . import views

urlpatterns = [
    # ex: /polls/
    url(r'^$', views.index, name='index'),
    # ex: /polls/2/ ... also repeated below with /detail/ suffix for convenience
    url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    # ex: /polls/2/detail/
    url(r'^(?P<question_id>[0-9]+)/detail/$', views.detail, name='detail'),
    # ex: /polls/2/results/
    url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    # ex: /polls/2/vote/
    url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
]

