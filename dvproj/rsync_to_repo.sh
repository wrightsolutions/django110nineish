#!/bin/sh
# Run rsync in test or live 
# Expects to be run in parent of dvproj repo as shown next
# sh dvproj/rsync_to_repo.sh 'test'
#
MODE_='sync'
if [ $# -gt 0 ]; then
    if [ ! -z "$1" ]; then
	MODE_=$1
    fi
fi
PATHEND_=$(basename "`pwd`")
if [ "xdvproj" = "x${PATHEND_}" ]; then
    # Being at 'dvproj' probably means we are in the directory itself (less than desirable)
    MODE_='test'
    printf 'Uncertain of being in correct place/path so running in mode %s\n' "${MODE_}"
else
    printf 'Running in mode %s\n' "${MODE_}"
fi
if [ "xsync" = "x${MODE_}" ]; then
    # Do the sync
    DT_=$(/bin/date +%Y%m%dT%H%M)
    printf 'Sync started at %s\n' "${DT_}"
    rsync -v -t -ogp -r  /var/www/html/django110/dvproj/ dvproj
else
    # Do the sync in preview mode (--dry-run) only
    printf 'Sync (in preview) started...\n'
    rsync -x -r --itemize-changes -ogp --dry-run /var/www/html/django110/dvproj/ dvproj
    printf 'Sync (in preview) fast completed.\n'
fi
DT_=$(/bin/date +%Y%m%dT%H%M)
printf 'Sync completed at %s\n' "${DT_}"
